# easyunits

Type-safe units for Rust:
 - capacity
 - distance
 - weight
 - currency
 - temperature
